#############################################
#
# Script for Siril 1.2
# February 2024
# (C) WA (Bill) Flowers
# WAF-HDR_brightness_weighted v2.0
#
# Version 2 brings out more detail in bright regions and also in dim sections
# It appears that best results are obtained if the background is color corrected
# and green noise is removed before applying the script.
# However it will work on unprocessed images and produce a reasonable result.
# Try it both ways to see what you like best.
#
# The work is based on a description of how to do this with layers in GIMP
#   https://www.instructables.com/HDR-photos-with-the-GIMP/
#
#### BRIGHTNESS WEIGHTED HDR COMPOSITING ####
# In an empty directory put images named short, medium and long
# representing short exposure to bring out bright details,
# medium exposure for mid-range features
# and long exposure to bring out dim features.
# They will be aligned, cropped and blended
# using brightness-weighted HDR compositing.

requires 1.2.0

# Build a sequence creating a Process sub-directory
convert HDRin_ -out=Process

# Perform a 2pass registration
cd Process
register HDRin_ -2pass

# Crop the 3 images to their common area
seqapplyreg HDRin_ -framing=min

# long will be 00001
# medium will be 00002
# short will be 00003

# get CieLAB luminance for long and short as Llong and Lshort
# a and b are ignored
load r_HDRin_00001
split Llong a b -lab
load Llong
autostretch
save Llong

load r_HDRin_00003
split Lshort a b -lab
load Lshort
autostretch
save Lshort

# compose Lshort into 3 (RGB) layers as the short exposure weight
rgbcomp Lshort Lshort Lshort -out=Wshort

# apply short exposure weight to medium and short images and save as HDRsm
pm "~$Wshort$ * $r_HDRin_00002$ + $Wshort$ * $r_HDRin_00003$"
save HDRsm

# compose Llong into 3 (RGB) layers as the negative long exposure weight
rgbcomp Llong Llong Llong -out=Wlong

# apply long exposure weight to HDRsm and long images and save as HDR in parent directory
# this pixelmath equation could have been combined with the previous
# which would avoid producing an intermediate output file
# however having the intermediate output file (HDRsm) is a useful check on the results
# and does not add significantly to the execution time
pm "$Wlong$ * $HDRsm$ + ~$Wlong$ * $r_HDRin_00001$"
cd ..
save HDR
