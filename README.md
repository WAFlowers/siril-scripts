# External Script Repository

Welcome to the External Script Repository! This repository is a collection of 
scripts contributed by users like you to enhance various stages of image 
manipulation and processing. Whether you're looking to streamline pre-processing 
tasks or optimize complex processing operations, you'll likely find a helpful 
script here.

## Categories

The scripts in this repository are neatly organized into two main categories:

### 1. Image Pre-processing

In the image pre-processing category, you will find scripts tailored to prepare 
and enhance astronomical images for further analysis and processing. This 
involves a series of essential steps, including:

- **Calibration:** Applying corrections using dark frames, bias frames, and flat 
  frames to eliminate sensor noise and irregularities.
- **Normalization:** Ensuring consistent pixel values across frames, laying the 
  groundwork for accurate comparisons.
- **Image Alignment:** Precisely aligning images to compensate for slight shifts 
  and rotations caused by factors like Earth's rotation.
- **Image Stacking:** Combining multiple aligned images to reduce noise and 
  enhance details.

These scripts are indispensable for laying a solid foundation for your 
astrophotography data, ensuring that your images are properly prepared for 
subsequent processing.

### 2. Image Processing

The image processing category encompasses scripts designed to extract meaningful 
insights from pre-processed astronomical images. This phase involves:

- **Enhancements:** Applying various filters, enhancements, and adjustments to 
  highlight structures and features within the images.
- **Feature Extraction:** Identifying and isolating specific elements of 
  interest, such as stars, galaxies, or nebulae.
- **Analysis:** Performing advanced statistical analyses and calculations to 
  derive quantitative information from the images.
- **Visualization:** Creating visual representations of your data, aiding in the 
  interpretation and communication of your findings.

## Contribution

We believe in the power of collaboration, and that's why we invite all users to 
contribute to this repository by submitting their own scripts via Merge 
Requests. Your contributions can span either category—pre-processing or 
processing—as long as they bring value and relevance to the table.

To submit your script, follow these steps:
1. Fork this repository to your gitlab account.
2. Create a new branch for your script.
3. Add your script to the appropriate category directory.
4. Commit your changes and push them to your fork.
5. Open a Merge Request detailing the purpose and functionality of your script.

**It is not necessary to apply to join the FA / siril-scripts project or to have special permission. Simply opening a merge request on your fork is enough.**

Our team will review your submission and determine its relevance to the repository's 
goals. If your script aligns with our objectives, we'll gladly merge it into the 
repository, giving you credit for your valuable contribution.

## Getting Started

If you're new to using scripts from this repository, here's how you can get 
started:

1. Clone the repository to your local machine.
2. Browse the categories to find a script that suits your needs.
3. Follow the instructions within the script's README, if provided.
4. Feel free to modify and customize the scripts to fit your specific requirements.

Remember, while these scripts can save you time and effort, it's important to 
understand what each script does before implementing it in your workflow.

Thank you for being a part of our growing community of Siril script enthusiasts! 
Together, we can make image processing more efficient and enjoyable.

*Note: This README is subject to change as the repository evolves. Be sure to 
check back for updates

